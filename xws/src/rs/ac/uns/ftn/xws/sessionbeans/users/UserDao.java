package rs.ac.uns.ftn.xws.sessionbeans.users;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import org.apache.log4j.Logger;

import rs.ac.uns.ftn.xws.entities.users.User;
import rs.ac.uns.ftn.xws.sessionbeans.common.GenericDaoBean;

@Stateless
@Local(UserDaoLocal.class)
public class UserDao extends GenericDaoBean<User, Long> implements UserDaoLocal{

	@Context
	private HttpServletRequest request;

	
	private static Logger log = Logger.getLogger(UserDao.class);
	
	@Override
	public User login(String username, String password){
		log.info("username: "+username);
		log.info("password: "+password);
		Query q = em.createQuery("select distinct u from " +
				"User u where u.username = :username " + 
				"and u.password = :password");
		q.setParameter("username", username);
		q.setParameter("password", password);
		@SuppressWarnings("unchecked")
		List<User> users = q.getResultList();
		if (users.size() == 1){
			request.getSession().setAttribute("user", users.get(0));
			return users.get(0);
		}
		else
			return null;
	}
	
	@Override
	public void logout(){
		log.info("LOGOUT");
		request.getSession().invalidate();
		
	}
}
