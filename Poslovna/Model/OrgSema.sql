/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     5/22/2015 22:02:31                           */
/*==============================================================*/


if exists (select 1
            from  sysindexes
           where  id    = object_id('ANALITIKA_MAGACINSKE_KARTICE')
            and   name  = 'STAVKA_DOKUMENTA_KOJA_JE_KREIRALA_STAVKU_ANALITIKE_MAG__KARTICE_FK'
            and   indid > 0
            and   indid < 255)
   drop index ANALITIKA_MAGACINSKE_KARTICE.STAVKA_DOKUMENTA_KOJA_JE_KREIRALA_STAVKU_ANALITIKE_MAG__KARTICE_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ANALITIKA_MAGACINSKE_KARTICE')
            and   name  = 'RELATIONSHIP_1_FK'
            and   indid > 0
            and   indid < 255)
   drop index ANALITIKA_MAGACINSKE_KARTICE.RELATIONSHIP_1_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ANALITIKA_MAGACINSKE_KARTICE')
            and   type = 'U')
   drop table ANALITIKA_MAGACINSKE_KARTICE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ARTIKAL')
            and   name  = 'RELATIONSHIP_6_FK'
            and   indid > 0
            and   indid < 255)
   drop index ARTIKAL.RELATIONSHIP_6_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ARTIKAL')
            and   type = 'U')
   drop table ARTIKAL
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CLAN')
            and   name  = 'RELATIONSHIP_22_FK'
            and   indid > 0
            and   indid < 255)
   drop index CLAN.RELATIONSHIP_22_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CLAN')
            and   name  = 'RELATIONSHIP_21_FK'
            and   indid > 0
            and   indid < 255)
   drop index CLAN.RELATIONSHIP_21_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CLAN')
            and   type = 'U')
   drop table CLAN
go

if exists (select 1
            from  sysobjects
           where  id = object_id('GRUPA_ARTIKALA')
            and   type = 'U')
   drop table GRUPA_ARTIKALA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MAGACIN')
            and   name  = 'RELATIONSHIP_7_FK'
            and   indid > 0
            and   indid < 255)
   drop index MAGACIN.RELATIONSHIP_7_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MAGACIN')
            and   type = 'U')
   drop table MAGACIN
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MAGACINSKA_KARTICA')
            and   name  = 'RELATIONSHIP_4_FK'
            and   indid > 0
            and   indid < 255)
   drop index MAGACINSKA_KARTICA.RELATIONSHIP_4_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MAGACINSKA_KARTICA')
            and   name  = 'RELATIONSHIP_3_FK'
            and   indid > 0
            and   indid < 255)
   drop index MAGACINSKA_KARTICA.RELATIONSHIP_3_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MAGACINSKA_KARTICA')
            and   name  = 'RELATIONSHIP_2_FK'
            and   indid > 0
            and   indid < 255)
   drop index MAGACINSKA_KARTICA.RELATIONSHIP_2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MAGACINSKA_KARTICA')
            and   type = 'U')
   drop table MAGACINSKA_KARTICA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('POPISNI_DOKUMENT')
            and   name  = 'RELATIONSHIP_18_FK'
            and   indid > 0
            and   indid < 255)
   drop index POPISNI_DOKUMENT.RELATIONSHIP_18_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('POPISNI_DOKUMENT')
            and   name  = 'RELATIONSHIP_17_FK'
            and   indid > 0
            and   indid < 255)
   drop index POPISNI_DOKUMENT.RELATIONSHIP_17_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('POPISNI_DOKUMENT')
            and   type = 'U')
   drop table POPISNI_DOKUMENT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('POSLOVNA_GODINA')
            and   name  = 'RELATIONSHIP_5_FK'
            and   indid > 0
            and   indid < 255)
   drop index POSLOVNA_GODINA.RELATIONSHIP_5_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('POSLOVNA_GODINA')
            and   type = 'U')
   drop table POSLOVNA_GODINA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('POSLOVNI_PARTNER')
            and   type = 'U')
   drop table POSLOVNI_PARTNER
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PREDUZECE')
            and   type = 'U')
   drop table PREDUZECE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PROMETNI_DOKUMENT')
            and   name  = 'RELATIONSHIP_14_FK'
            and   indid > 0
            and   indid < 255)
   drop index PROMETNI_DOKUMENT.RELATIONSHIP_14_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PROMETNI_DOKUMENT')
            and   name  = 'KUPAC_ILI_DOBAVLJAC_FK'
            and   indid > 0
            and   indid < 255)
   drop index PROMETNI_DOKUMENT.KUPAC_ILI_DOBAVLJAC_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PROMETNI_DOKUMENT')
            and   name  = 'ZA_MEDJUMAGACINSKI_TRANSFER___ULAZNI_FK'
            and   indid > 0
            and   indid < 255)
   drop index PROMETNI_DOKUMENT.ZA_MEDJUMAGACINSKI_TRANSFER___ULAZNI_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PROMETNI_DOKUMENT')
            and   name  = 'MAGACIN_U_PROMETU_FK'
            and   indid > 0
            and   indid < 255)
   drop index PROMETNI_DOKUMENT.MAGACIN_U_PROMETU_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PROMETNI_DOKUMENT')
            and   type = 'U')
   drop table PROMETNI_DOKUMENT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('RADNIK')
            and   name  = 'RELATIONSHIP_23_FK'
            and   indid > 0
            and   indid < 255)
   drop index RADNIK.RELATIONSHIP_23_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('RADNIK')
            and   type = 'U')
   drop table RADNIK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('STAVKA_PROMETNOG_DOKUMENTA')
            and   name  = 'RELATIONSHIP_13_FK'
            and   indid > 0
            and   indid < 255)
   drop index STAVKA_PROMETNOG_DOKUMENTA.RELATIONSHIP_13_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('STAVKA_PROMETNOG_DOKUMENTA')
            and   name  = 'RELATIONSHIP_12_FK'
            and   indid > 0
            and   indid < 255)
   drop index STAVKA_PROMETNOG_DOKUMENTA.RELATIONSHIP_12_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('STAVKA_PROMETNOG_DOKUMENTA')
            and   type = 'U')
   drop table STAVKA_PROMETNOG_DOKUMENTA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('STAVKE_POPISA')
            and   name  = 'RELATIONSHIP_15_FK'
            and   indid > 0
            and   indid < 255)
   drop index STAVKE_POPISA.RELATIONSHIP_15_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('STAVKE_POPISA')
            and   type = 'U')
   drop table STAVKE_POPISA
go

if exists(select 1 from systypes where name='DOMAIN_1')
   drop type DOMAIN_1
go

/*==============================================================*/
/* Domain: DOMAIN_1                                             */
/*==============================================================*/
create type DOMAIN_1
   from char(50)
go

/*==============================================================*/
/* Table: ANALITIKA_MAGACINSKE_KARTICE                          */
/*==============================================================*/
create table ANALITIKA_MAGACINSKE_KARTICE (
   ANALITIKAMAGKARID    int                  not null,
   STAVKAPROMDOKID      int                  null,
   MAGACINSKAKARTICAID  int                  not null,
   DATUM_PROMENE        datetime             null,
   VRSTA_DOKUMENTA      char(2)              null
      constraint CKC_VRSTA_DOKUMENTA_ANALITIK check (VRSTA_DOKUMENTA is null or (VRSTA_DOKUMENTA in ('pr','ot','mm','ni','kp','ps'))),
   SIFRA_DOKUMENTA      varchar(12)          null,
   KOLICINA             decimal(15,2)        null,
   VREDNOST             decimal(15,2)        null,
   SMER                 char(1)              null
      constraint CKC_SMER_ANALITIK check (SMER is null or (SMER in ('U','I'))),
   constraint PK_ANALITIKA_MAGACINSKE_KARTIC primary key nonclustered (ANALITIKAMAGKARID)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_1_FK on ANALITIKA_MAGACINSKE_KARTICE (
MAGACINSKAKARTICAID ASC
)
go

/*===========================================================================*/
/* Index: STAVKA_DOKUMENTA_KOJA_JE_KREIRALA_STAVKU_ANALITIKE_MAG__KARTICE_FK */
/*===========================================================================*/
create index STAVKA_DOKUMENTA_KOJA_JE_KREIRALA_STAVKU_ANALITIKE_MAG__KARTICE_FK on ANALITIKA_MAGACINSKE_KARTICE (
STAVKAPROMDOKID ASC
)
go

/*==============================================================*/
/* Table: ARTIKAL                                               */
/*==============================================================*/
create table ARTIKAL (
   ARTIKALID            int                  not null,
   GRUPAID              int                  not null,
   SIFRA_ARTIKALA       bigint               not null,
   PAKOVANJE            decimal(10,2)        null,
   JEDINICA_MERE        char(3)              null
      constraint CKC_JEDINICA_MERE_ARTIKAL check (JEDINICA_MERE is null or (JEDINICA_MERE in ('kg','<Val4>','<Val5>','<Val6>','<Val7>'))),
   NAZIV_ARTIKLA        varchar(1024)        null,
   constraint PK_ARTIKAL primary key nonclustered (ARTIKALID),
   constraint AK_IDENTIFIER_2_ARTIKAL unique (SIFRA_ARTIKALA)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_6_FK on ARTIKAL (
GRUPAID ASC
)
go

/*==============================================================*/
/* Table: CLAN                                                  */
/*==============================================================*/
create table CLAN (
   CLANID               int                  not null,
   POPISNIDOKID         int                  not null,
   RADNIKID             char(10)             not null,
   ULOGA                char(1)              null
      constraint CKC_ULOGA_CLAN check (ULOGA is null or (ULOGA in ('p','o'))),
   constraint PK_CLAN primary key nonclustered (CLANID)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_21_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_21_FK on CLAN (
POPISNIDOKID ASC
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_22_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_22_FK on CLAN (
RADNIKID ASC
)
go

/*==============================================================*/
/* Table: GRUPA_ARTIKALA                                        */
/*==============================================================*/
create table GRUPA_ARTIKALA (
   GRUPAID              int                  not null,
   SIFRA_GRUPE          int                  not null,
   NAZIV_GRUPE          varchar(55)          null,
   constraint PK_GRUPA_ARTIKALA primary key nonclustered (GRUPAID),
   constraint AK_IDENTIFIER_2_GRUPA_AR unique (SIFRA_GRUPE)
)
go

/*==============================================================*/
/* Table: MAGACIN                                               */
/*==============================================================*/
create table MAGACIN (
   MAGACINID            int                  not null,
   PREDUZECEID          int                  not null,
   SIFRA_MAGACINA       int                  not null,
   NAZIV_MAGACINA       varchar(255)         null,
   constraint PK_MAGACIN primary key nonclustered (MAGACINID),
   constraint AK_IDENTIFIER_2_MAGACIN unique (SIFRA_MAGACINA)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_7_FK on MAGACIN (
PREDUZECEID ASC
)
go

/*==============================================================*/
/* Table: MAGACINSKA_KARTICA                                    */
/*==============================================================*/
create table MAGACINSKA_KARTICA (
   MAGACINSKAKARTICAID  int                  not null,
   ARTIKALID            int                  not null,
   PREDUZECEID          int                  not null,
   POSLOVNAGODINAID     int                  not null,
   MAGACINID            int                  not null,
   OZNAKA_KARTICE       int                  not null,
   PROSECNA_CENA        decimal(12,2)        null,
   KOLICINA_ULAZA       decimal(15,2)        null,
   VREDNOST_ULAZA       decimal(15,2)        null,
   KOLICINA_IZLAZA      decimal(15,2)        null,
   VREDNOST_IZLAZA      decimal(15,2)        null,
   constraint PK_MAGACINSKA_KARTICA primary key nonclustered (MAGACINSKAKARTICAID),
   constraint AK_IDENTIFIER_2_MAGACINS unique (OZNAKA_KARTICE)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_2_FK on MAGACINSKA_KARTICA (
MAGACINID ASC
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_3_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_3_FK on MAGACINSKA_KARTICA (
ARTIKALID ASC
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_4_FK on MAGACINSKA_KARTICA (
PREDUZECEID ASC,
POSLOVNAGODINAID ASC
)
go

/*==============================================================*/
/* Table: POPISNI_DOKUMENT                                      */
/*==============================================================*/
create table POPISNI_DOKUMENT (
   POPISNIDOKID         int                  not null,
   PREDUZECEID          int                  not null,
   POSLOVNAGODINAID     int                  not null,
   MAGACINID            int                  not null,
   DATUM_POPISA         datetime             null,
   STATUS               char(1)              null
      constraint CKC_STATUS_POPISNI_ check (STATUS is null or (STATUS in ('f','p','s'))),
   constraint PK_POPISNI_DOKUMENT primary key nonclustered (POPISNIDOKID)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_17_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_17_FK on POPISNI_DOKUMENT (
PREDUZECEID ASC,
POSLOVNAGODINAID ASC
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_18_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_18_FK on POPISNI_DOKUMENT (
MAGACINID ASC
)
go

/*==============================================================*/
/* Table: POSLOVNA_GODINA                                       */
/*==============================================================*/
create table POSLOVNA_GODINA (
   PREDUZECEID          int                  not null,
   POSLOVNAGODINAID     int                  not null,
   GODINA               datetime             not null,
   ZAKLJUCENA           bit                  null,
   constraint PK_POSLOVNA_GODINA primary key nonclustered (PREDUZECEID, POSLOVNAGODINAID)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_5_FK on POSLOVNA_GODINA (
PREDUZECEID ASC
)
go

/*==============================================================*/
/* Table: POSLOVNI_PARTNER                                      */
/*==============================================================*/
create table POSLOVNI_PARTNER (
   POSLOVNIPARTNERID    int                  not null,
   PIB_PARTNERA         bigint               not null,
   NAZIV_PARTNERA       varchar(255)         null,
   ADRESA               varchar(255)         null,
   TELEFON              varchar(55)          null,
   EMAIL                varchar(255)         null,
   constraint PK_POSLOVNI_PARTNER primary key nonclustered (POSLOVNIPARTNERID),
   constraint AK_IDENTIFIER_2_POSLOVNI unique (PIB_PARTNERA)
)
go

/*==============================================================*/
/* Table: PREDUZECE                                             */
/*==============================================================*/
create table PREDUZECE (
   PREDUZECEID          int                  not null,
   PIB                  bigint               not null,
   NAZIV_PREDUZECA      varchar(55)          null,
   ADRESA               varchar(255)         null,
   TELEFON              varchar(55)          null,
   MAIL                 varchar(55)          null,
   constraint PK_PREDUZECE primary key nonclustered (PREDUZECEID),
   constraint AK_IDENTIFIER_2_PREDUZEC unique (PIB)
)
go

/*==============================================================*/
/* Table: PROMETNI_DOKUMENT                                     */
/*==============================================================*/
create table PROMETNI_DOKUMENT (
   PROMETNIDOKUMENTID   int                  not null,
   MAGACINID            int                  not null,
   PREDUZECEID          int                  not null,
   POSLOVNAGODINAID     int                  not null,
   POSLOVNIPARTNERID    int                  null,
   MAG_MAGACINID        int                  null,
   REDNI_BROJ_DOKUMENTA int                  not null,
   DATUM_NASTANKA       datetime             null,
   DATUM_KNJIZENJA      datetime             null,
   STATUS               char(1)              null
      constraint CKC_STATUS_PROMETNI check (STATUS is null or (STATUS in ('f','p','s'))),
   constraint PK_PROMETNI_DOKUMENT primary key nonclustered (PROMETNIDOKUMENTID),
   constraint AK_IDENTIFIER_2_PROMETNI unique (REDNI_BROJ_DOKUMENTA)
)
go

/*==============================================================*/
/* Index: MAGACIN_U_PROMETU_FK                                  */
/*==============================================================*/
create index MAGACIN_U_PROMETU_FK on PROMETNI_DOKUMENT (
MAGACINID ASC
)
go

/*==============================================================*/
/* Index: ZA_MEDJUMAGACINSKI_TRANSFER___ULAZNI_FK               */
/*==============================================================*/
create index ZA_MEDJUMAGACINSKI_TRANSFER___ULAZNI_FK on PROMETNI_DOKUMENT (
MAG_MAGACINID ASC
)
go

/*==============================================================*/
/* Index: KUPAC_ILI_DOBAVLJAC_FK                                */
/*==============================================================*/
create index KUPAC_ILI_DOBAVLJAC_FK on PROMETNI_DOKUMENT (
POSLOVNIPARTNERID ASC
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_14_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_14_FK on PROMETNI_DOKUMENT (
PREDUZECEID ASC,
POSLOVNAGODINAID ASC
)
go

/*==============================================================*/
/* Table: RADNIK                                                */
/*==============================================================*/
create table RADNIK (
   RADNIKID             char(10)             not null,
   CLANID               int                  null,
   IME                  varchar(55)          null,
   PREZIME              varchar(55)          null,
   KORISNICKO_IME       varchar(55)          null,
   LOZINKA              varchar(55)          null,
   constraint PK_RADNIK primary key nonclustered (RADNIKID)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_23_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_23_FK on RADNIK (
CLANID ASC
)
go

/*==============================================================*/
/* Table: STAVKA_PROMETNOG_DOKUMENTA                            */
/*==============================================================*/
create table STAVKA_PROMETNOG_DOKUMENTA (
   STAVKAPROMDOKID      int                  not null,
   ARTIKALID            int                  not null,
   PROMETNIDOKUMENTID   int                  not null,
   REDNI_BROJ_STAVKE    int                  not null,
   KOLICINA             decimal(15,2)        null,
   VREDNOST             decimal(15,2)        null,
   CENA_STAVKE          decimal(12,2)        null,
   constraint PK_STAVKA_PROMETNOG_DOKUMENTA primary key nonclustered (STAVKAPROMDOKID)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_12_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_12_FK on STAVKA_PROMETNOG_DOKUMENTA (
ARTIKALID ASC
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_13_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_13_FK on STAVKA_PROMETNOG_DOKUMENTA (
PROMETNIDOKUMENTID ASC
)
go

/*==============================================================*/
/* Table: STAVKE_POPISA                                         */
/*==============================================================*/
create table STAVKE_POPISA (
   STAVKAPOPISAID       int                  not null,
   POPISNIDOKID         int                  not null,
   KOLICINA_PO_POPISU   decimal(20,3)        null,
   KOLICINA_U_KARTICI   decimal(20,3)        null,
   CENA_STAVKE          decimal(12,2)        null,
   constraint PK_STAVKE_POPISA primary key nonclustered (STAVKAPOPISAID)
)
go

/*==============================================================*/
/* Index: RELATIONSHIP_15_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_15_FK on STAVKE_POPISA (
POPISNIDOKID ASC
)
go

