package session;

import java.util.List;

import rs.ac.uns.ftn.xws.faktura.Faktura;
import rs.ac.uns.ftn.xws.firma.Firma;

public interface FirmaDaoLocal extends GenericDaoLocal<Firma,Long> {


}
