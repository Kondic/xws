package session;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;

import rs.ac.uns.ftn.xws.faktura.Faktura;



public interface FakturaDaoLocal extends GenericDaoLocal<Faktura,Long> {
	
	
	public  List<Faktura> findAllByDobavljac(String idKupca,String idDobavljaca) throws IOException, JAXBException;

}
