package session;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.basex.rest.Result;
import org.basex.rest.Results;
import org.w3c.dom.Node;

import rs.ac.uns.ftn.xws.faktura.Faktura;


@Stateless
@Local(FakturaDaoLocal.class)
public class FakturaDaoBean extends GenericDaoBean<Faktura,Long> implements FakturaDaoLocal
{	
	
	
	
	
	public static final String contextPath = "rs.ac.uns.ftn.xws.faktura";
	
	public static final String schemaName = "fakture";
	
	public FakturaDaoBean() {
		super(contextPath,schemaName);
	}

	@Override
	public List<Faktura> findAllByDobavljac(String idKupca, String idDobavljaca) throws IOException, JAXBException {
	
	/*	ArrayList<Faktura> fakture=new ArrayList<Faktura>();
		fakture=(ArrayList<Faktura>) findAll();
		System.out.println(fakture.size());*/
	/*	return  fakture;*/
	
		ArrayList<Faktura> fakture=new ArrayList<Faktura>();
		ArrayList<Faktura> rezultat=new ArrayList<Faktura>();
		fakture=(ArrayList<Faktura>) findAll();
		for(Faktura f:fakture) {
			if(f.getZaglavljeFakture().getPibKupca().equals(idKupca) &&  f.getZaglavljeFakture().getPibDobavljaca().equals(idDobavljaca)) {
				
				
				rezultat.add(f);
				
				
			}
			
		}
		//collection('fakture')
		/*String xQuery="collection('fakture')//faktura";*/
/*   String query = "(//faktura[//zaglavlje_fakture[pib_dobavljaca=2]])";
   query.replace(" ", "+");*/
	
		/*String xQuery="//faktura";*/
		
		
/*		InputStream resultStream=findBy(xQuery,true);
	if(resultStream!=null) {
		Unmarshaller unmarshaller;
		JAXBContext context;
		context=JAXBContext.newInstance(contextPath);
		unmarshaller=context.createUnmarshaller();
		
		
		
		Results wrappedResults = (Results) em.getBasex_unmarshaller().unmarshal(resultStream);
		for (Result result : wrappedResults.getResult()) {
			
			fakture.add((Faktura) unmarshaller.unmarshal((Node)result.getAny()));
		}
	}	
		return fakture;*/
		return rezultat;
	}


	
	
	
	

}
