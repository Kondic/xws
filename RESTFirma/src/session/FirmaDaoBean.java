package session;

import javax.ejb.Local;
import javax.ejb.Stateless;

import rs.ac.uns.ftn.xws.firma.Firma;


@Stateless
@Local(FirmaDaoLocal.class)
public class FirmaDaoBean extends GenericDaoBean<Firma,Long> implements FirmaDaoLocal {

	
	
	public static final String contextPath = "rs.ac.uns.ftn.xws.firma";
	
	public static final String schemaName = "firme";
	
	public FirmaDaoBean() {
		super(contextPath,schemaName);
		
	}
	
	
}
