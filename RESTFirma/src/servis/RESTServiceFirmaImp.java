package servis;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.JAXBException;

import rs.ac.uns.ftn.xws.faktura.Faktura;
import rs.ac.uns.ftn.xws.firma.Firma;
import session.FakturaDaoLocal;
import session.FirmaDaoLocal;




@Stateless
public class RESTServiceFirmaImp implements RESTServiceFirma {
	
	
	private String firmaId="12";
	
	 @EJB
	 private FakturaDaoLocal fakturaDAO;
	
	 @EJB
	 private FirmaDaoLocal firmaDAO;
	

  public String getIdFirme() {
	return firmaId;
}





public void setIdFirme(String idFirme) {
	this.firmaId = idFirme;
}

@Override
public Response getFakture(String id) {
	
   Firma firma=null;	
 	try {
		firma=firmaDAO.findById(Long.valueOf(firmaId));
	} catch (NumberFormatException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (JAXBException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
   
	
 	if(firma==null ){
 		return Response.status(Status.NOT_FOUND).build(); 		
 	}else {
 		
 		Boolean isPoslovniPartner=false;
 	   for(String s:firma.getPoslovnipartneri()){
 		   //System.out.println(s);
 		   if(id.equals(s)) {
 			   isPoslovniPartner=true;
 		   }
 	   }
 	   
 	   
 	   if(!isPoslovniPartner) {
 		  return Response.status(Status.NOT_FOUND).build(); 		   
 	   }
 		
 	
	List<Faktura> f=new ArrayList<Faktura>();
	try {
		f = fakturaDAO.findAllByDobavljac(firmaId,id);
	/*	for(Faktura fa:f) {
			System.out.println(fa.getId());
		} */
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (JAXBException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	 return Response.status(200).entity(f).build();

 	}
	
}

@Override
public Response getFaktura(String id, String idFakture) {
	
/*	if((!f.getZaglavljeFakture().getPibDobavljaca().equals(id)) || (!f.getZaglavljeFakture().getPibKupca().equals(firmaId)) ) {
		return  Response.status(Status.NOT_FOUND).build();
		}*/
	
	Firma firma=null;	
 	try {
		firma=firmaDAO.findById(Long.valueOf(firmaId));
	} catch (NumberFormatException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (JAXBException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
 	
 	
 	
 	
 	
	if(firma==null ){
		return Response.status(Status.NOT_FOUND).build(); 	
	}else {
		Boolean isPoslovniPartner=false;
	 	   for(String s:firma.getPoslovnipartneri()){
	 		   if(id.equals(s)) {
	 			   isPoslovniPartner=true;
	 		   }
	 	   }
	 	   
	 	   
	 	   
	 	   if(!isPoslovniPartner) {
	 		  return Response.status(Status.NOT_FOUND).build(); 		   
	 	   }
	 		
	 	   if(Long.valueOf(idFakture)!=null) {
	 	   try {
			Faktura f=fakturaDAO.findById(Long.valueOf(idFakture));
			if(f!=null){
				if((!f.getZaglavljeFakture().getPibDobavljaca().equals(id)) || (!f.getZaglavljeFakture().getPibKupca().equals(firmaId)) ) {
					return  Response.status(Status.NOT_FOUND).build();
					}
				return Response.ok().entity(f).build();
				
				
				
				
				
				
				
			}
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 	   
	 	   }else {
	 		  return Response.status(Status.NOT_FOUND).build(); 
	 		   
	 	   }
	 	   
		
		
		
		
	}
	
	
	
	// TODO Auto-generated method stub
	return null;
}

@Override
public Response getStavke(String id, String idFakture) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Response addStavka(String id, String idFakture) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Response updateStavka(String id, String idFakture, String idStavke) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Response removeStavka(String id, String idFakture, String idStavke) {
	// TODO Auto-generated method stub
	return null;
}





@Override
public Response addFaktura(String id, Faktura f) {
	
	Firma firma=null;
	
	if((!f.getZaglavljeFakture().getPibDobavljaca().equals(id)) || (!f.getZaglavljeFakture().getPibKupca().equals(firmaId)) ) {
		return  Response.status(Status.FORBIDDEN).build();
		}
	
	
	
	try {
		
		 firma=firmaDAO.findById(Long.valueOf(firmaId));
	}
		 catch (JAXBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

	if(firma!=null) {
	Boolean isPoslovniPartner=false;
	   for(String s:firma.getPoslovnipartneri()){
		 /*  System.out.println(s);*/
		   if(f.getZaglavljeFakture().getPibDobavljaca().equals(s)) {
			   isPoslovniPartner=true;
		   }
	   }
	   
	
			
	   if(!isPoslovniPartner) {
	   return  Response.status(Status.FORBIDDEN).build();
	   }
			
			
	
	
	
	else {
	
	try {
		fakturaDAO.persist(f);
	} catch (JAXBException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	}else {
		return  Response.status(Status.FORBIDDEN).build();
		
	}
	// TODO Auto-generated method stub
	URI uri=URI.create("/partneri/"+id+"/fakture"+f.getId());
	
	return  Response.created(uri).build();
}





/*public static void main(final String[] args) {
	  
	  RESTServiceFirma f=new RESTServiceFirma();
	  System.out.println(f.getIdFirme());
  }

	private void init() {
		firmaId= ResourceBundle.getBundle("resources.firma").getString("idFirma");	
		
	}*/

}
