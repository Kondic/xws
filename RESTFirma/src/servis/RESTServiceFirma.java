package servis;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import rs.ac.uns.ftn.xws.faktura.Faktura;


@Path("/partneri")
public interface RESTServiceFirma {

	
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("{id}/fakture")
	public Response getFakture(@PathParam("id") String id) ; 
	
	
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
	@Path("{id}/fakture/")
	public Response  addFaktura(@PathParam("id") String id,Faktura f);
	
	@GET
	@Produces("application/xml")
	@Path("{id}/fakture/{idFakture}")
	public Response getFaktura(@PathParam("id") String id,@PathParam("idFakture") String idFakture);
	
	@GET
	@Produces("application/xml")
	@Path("{id}/fakture/{idFakture}/stavke")
	public Response  getStavke(@PathParam("id") String id,@PathParam("idFakture") String idFakture); 
	
	@POST
	@Path("{id}/fakture/{idFakture}/stavke")
	public Response  addStavka(@PathParam("id") String id,@PathParam("idFakture") String idFakture);
	
	@PUT
	@Path("{id}/fakture/{idFakture}/stavke/{idStavke}")
	public Response  updateStavka(@PathParam("id") String id,@PathParam("idFakture") String idFakture,@PathParam("idStavke") String idStavke);
	
	@DELETE
	@Path("{id}/fakture/{idFakture}/stavke/{idStavke}")
	public Response  removeStavka(@PathParam("id") String id,@PathParam("idFakture") String idFakture,@PathParam("idStavke") String idStavke);
	
	
	/*public Order getOrder(@PathParam ("orderId") int officeId);*/
	
}
