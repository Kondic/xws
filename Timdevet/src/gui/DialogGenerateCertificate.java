package gui;

import gui.tablemodel.KeySert;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.security.KeyPair;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map.Entry;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import rs.ac.uns.ftn.informatika.ib.security.CertificateGenerator;

public class DialogGenerateCertificate extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1847399076288450816L;
	/** Signature Algorithm combo box */
	private JComboBox m_jcbSigAlg;

	private static final String KEY_STORE_FILE = "./data";

	/** Surname text field */
	private JTextField m_jtfSurname;

	/** Common Name text field */
	private JTextField m_jtfCommonName;

	/** Organization Unit text field */
	private JTextField m_jtfOrganisationUnit;

	/** Organization Unit Name */
	private JTextField m_jtfOrganisationName;

	/** Locality Name text field */
	private JTextField m_jtfGivenName;

	/** State Name text field */
	private JTextField m_jtfStateName;

	/** Country Code text field */
	private JTextField m_jtfCountryCode;

	/** Email Address text field */
	private JTextField m_jtfEmailAddress;

	/** The key pair type */

	/** Entered common name */
	private String m_sCommonName;

	/** Entered organization unit */
	private String m_sOrganizationUnit;

	/** Entered organization name */
	private String m_sOrganizationName;

	/** Entered locality name */
	private String m_sGivenName;

	/** Entered state name */
	private String m_sStateName;

	/** Entered country code */
	private String m_sCountryCode;

	/** Entered e-mail address */
	private String m_sEmailAddress;

	/** Entered given name address */
	// private String m_sGivenName;

	private DialogAlias dialogAlias;
	private X509Certificate x509certificate;
	private DialogGetPassword dialogGetPassword;
	/**
	 * Creates new DGenerateCertificate dialog.
	 * 
	 * @param parent
	 *            The parent window
	 * @param sTitle
	 *            The dialog's title
	 * @param keyPairType
	 *            The key pair type
	 */
	public DialogGenerateCertificate(Window parent, String sTitle) {
		super(parent);

		initComponents(sTitle);
		setLocationRelativeTo(null);
		 ImageIcon icon = new ImageIcon(new ImageIcon("icons/2_application-pgp-signature.png").getImage());
	     setIconImage(icon.getImage());
	    
	}

	/**
	 * Initialize the dialog's GUI components.
	 * 
	 * @param sTitle
	 *            The dialog's title
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initComponents(String sTitle) {
		// TODO Auto-generated method stub
		GridBagConstraints gbcLbl = new GridBagConstraints();
		gbcLbl.gridx = 0;
		gbcLbl.gridwidth = 3;
		gbcLbl.gridheight = 1;
		gbcLbl.insets = new Insets(5, 5, 5, 5);
		gbcLbl.anchor = GridBagConstraints.EAST;

		GridBagConstraints gbcEdCtrl = new GridBagConstraints();
		gbcEdCtrl.gridx = 3;
		gbcEdCtrl.gridwidth = 3;
		gbcEdCtrl.gridheight = 1;
		gbcEdCtrl.insets = new Insets(5, 5, 5, 5);
		gbcEdCtrl.anchor = GridBagConstraints.WEST;

		int gridy = 0;

		// Signature Algorithm
		JLabel jlSigAlg = new JLabel("CA");
		GridBagConstraints gbc_jlSigAlg = (GridBagConstraints) gbcLbl.clone();
		gbc_jlSigAlg.gridy = gridy++;

		String[] comboBoxArray = allCN().toArray(new String[allCN().size()]);

		m_jcbSigAlg = new JComboBox(comboBoxArray);

		// ucitavamo podatke
		jlSigAlg.setLabelFor(m_jcbSigAlg);
		GridBagConstraints gbc_jcbSigAlg = (GridBagConstraints) gbcEdCtrl
				.clone();
		gbc_jcbSigAlg.gridy = gbc_jlSigAlg.gridy;

		// Validity Period
		JLabel jlValidity = new JLabel("Surname (S)");
		GridBagConstraints gbc_jlValidity = (GridBagConstraints) gbcLbl.clone();
		gbc_jlValidity.gridy = gridy++;

		m_jtfSurname = new JTextField(15);

		jlValidity.setLabelFor(m_jtfSurname);
		GridBagConstraints gbc_jtfValidity = (GridBagConstraints) gbcEdCtrl
				.clone();
		gbc_jtfValidity.gridy = gbc_jlValidity.gridy;

		// Common Name
		JLabel jlCommonName = new JLabel("Common name (CN)");
		GridBagConstraints gbc_jlCommonName = (GridBagConstraints) gbcLbl
				.clone();
		gbc_jlCommonName.gridy = gridy++;

		m_jtfCommonName = new JTextField(15);
		jlCommonName.setLabelFor(m_jtfCommonName);
		GridBagConstraints gbc_jtfCommonName = (GridBagConstraints) gbcEdCtrl
				.clone();
		gbc_jtfCommonName.gridy = gbc_jlCommonName.gridy;

		// Organization Unit
		JLabel jlOrganisationUnit = new JLabel("Organisation Unit (OU)");
		GridBagConstraints gbc_jlOrganisationUnit = (GridBagConstraints) gbcLbl
				.clone();
		gbc_jlOrganisationUnit.gridy = gridy++;

		m_jtfOrganisationUnit = new JTextField(15);
		jlOrganisationUnit.setLabelFor(m_jtfOrganisationUnit);
		GridBagConstraints gbc_jtfOrganisationUnit = (GridBagConstraints) gbcEdCtrl
				.clone();
		gbc_jtfOrganisationUnit.gridy = gbc_jlOrganisationUnit.gridy;

		// Organization Name
		JLabel jlOrganisationName = new JLabel(" OrganisationName (O)");
		GridBagConstraints gbc_jlOrganisationName = (GridBagConstraints) gbcLbl
				.clone();
		gbc_jlOrganisationName.gridy = gridy++;

		m_jtfOrganisationName = new JTextField(15);
		m_jtfOrganisationName.setToolTipText("OrganisationName (O)");
		jlOrganisationName.setLabelFor(m_jtfOrganisationName);
		GridBagConstraints gbc_jtfOrganisationName = (GridBagConstraints) gbcEdCtrl
				.clone();
		gbc_jtfOrganisationName.gridy = gbc_jlOrganisationName.gridy;

		// Locality Name
		JLabel jlLocalityName = new JLabel("Given Name ");
		GridBagConstraints gbc_jlLocalityName = (GridBagConstraints) gbcLbl
				.clone();
		gbc_jlLocalityName.gridy = gridy++;

		m_jtfGivenName = new JTextField(15);
		jlLocalityName.setLabelFor(m_jtfGivenName);
		GridBagConstraints gbc_jtfLocalityName = (GridBagConstraints) gbcEdCtrl
				.clone();
		gbc_jtfLocalityName.gridy = gbc_jlLocalityName.gridy;

		// State Name
		JLabel jlStateName = new JLabel("State Name ");
		GridBagConstraints gbc_jlStateName = (GridBagConstraints) gbcLbl
				.clone();
		gbc_jlStateName.gridy = gridy++;

		m_jtfStateName = new JTextField(15);
		jlStateName.setLabelFor(m_jtfStateName);
		GridBagConstraints gbc_jtfStateName = (GridBagConstraints) gbcEdCtrl
				.clone();
		gbc_jtfStateName.gridy = gbc_jlStateName.gridy;

		// Country Code
		JLabel jlCountryCode = new JLabel("CountryCode (C) ");
		GridBagConstraints gbc_jlCountryCode = (GridBagConstraints) gbcLbl
				.clone();
		gbc_jlCountryCode.gridy = gridy++;

		m_jtfCountryCode = new JTextField(Locale.getDefault().getCountry());

		jlCountryCode.setLabelFor(m_jtfCountryCode);
		GridBagConstraints gbc_jtfCountryCode = (GridBagConstraints) gbcEdCtrl
				.clone();
		gbc_jtfCountryCode.gridy = gbc_jlCountryCode.gridy;

		// Email Address
		JLabel jlEmailAddress = new JLabel("EmailAddress(E) ");
		GridBagConstraints gbc_jlEmailAddress = (GridBagConstraints) gbcLbl
				.clone();
		gbc_jlEmailAddress.gridy = gridy++;

		m_jtfEmailAddress = new JTextField(15);
		jlEmailAddress.setLabelFor(m_jtfEmailAddress);
		GridBagConstraints gbc_jtfEmailAddress = (GridBagConstraints) gbcEdCtrl
				.clone();
		gbc_jtfEmailAddress.gridy = gbc_jlEmailAddress.gridy;

		// Put it all together
		JPanel jpOptions = new JPanel(new GridBagLayout());
		jpOptions.setBorder(new CompoundBorder(new EmptyBorder(5, 5, 5, 5),
				new EtchedBorder()));

		jpOptions.add(jlSigAlg, gbc_jlSigAlg);
		jpOptions.add(m_jcbSigAlg, gbc_jcbSigAlg);
		jpOptions.add(jlValidity, gbc_jlValidity);
		jpOptions.add(m_jtfSurname, gbc_jtfValidity);
		jpOptions.add(jlCommonName, gbc_jlCommonName);
		jpOptions.add(m_jtfCommonName, gbc_jtfCommonName);
		jpOptions.add(jlOrganisationUnit, gbc_jlOrganisationUnit);
		jpOptions.add(m_jtfOrganisationUnit, gbc_jtfOrganisationUnit);
		jpOptions.add(jlOrganisationName, gbc_jlOrganisationName);
		jpOptions.add(m_jtfOrganisationName, gbc_jtfOrganisationName);
		jpOptions.add(jlLocalityName, gbc_jlLocalityName);
		jpOptions.add(m_jtfGivenName, gbc_jtfLocalityName);
		jpOptions.add(jlStateName, gbc_jlStateName);
		jpOptions.add(m_jtfStateName, gbc_jtfStateName);
		jpOptions.add(jlCountryCode, gbc_jlCountryCode);
		jpOptions.add(m_jtfCountryCode, gbc_jtfCountryCode);
		jpOptions.add(jlEmailAddress, gbc_jlEmailAddress);
		jpOptions.add(m_jtfEmailAddress, gbc_jtfEmailAddress);

		JButton jbOK = new JButton("OK");
		jbOK.addActionListener(new ActionListener() {

			 
			public void actionPerformed(ActionEvent arg0) {
				// GENNERISANJE SERTIFIKATA, i da udjemo u keystore za alias
				// trebalo bi doddavanje na neki alias
				if (validateValues()) {

					SimpleDateFormat iso8601Formater = new SimpleDateFormat(
							"yyyy-MM-dd");
					Date startDate = null;
					try {
						startDate = iso8601Formater.parse("2007-12-31");
					} catch (ParseException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					Date endDate = null;
					try {
						endDate = iso8601Formater.parse("2017-12-31");
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					String ca=null;
					if(m_jcbSigAlg.getSelectedItem()!=null)
					  ca = m_jcbSigAlg.getSelectedItem().toString().trim();
					//String uid = m_jtfSurname.getText();
					String cn = m_jtfCommonName.getText();
					String surname = m_jtfSurname.getText();
					String givenname = m_jtfGivenName.getText();
					String o = m_jtfOrganisationName.getText();
					String ou = m_jtfOrganisationUnit.getText();
					String c = m_jtfCountryCode.getText();
					String e = m_jtfEmailAddress.getText();

					CertificateGenerator cg = new CertificateGenerator();
					KeyPair keyPair = cg.generateKeyPair();

					x509certificate = cg.generateSsCertificateFromScratch(ca,
							startDate, endDate , cn, surname, givenname, o,
							ou, c, e, keyPair);
					cg.saveCert(x509certificate, givenname);
					System.out.println("OVO JE CA:"+ca+"!");
					if (ca == null || ca.equals("") ) {
						//novi keyystore da ubacimo CA
						System.out.println("novi keyystore da ubacimo CA");
						dialogAlias = new DialogAlias(MainFrame.getInstance(),
								"Adding Alias", x509certificate, keyPair,
								givenname);
						dialogAlias.setVisible(true);
 					}
 						else {
 							System.out.println("novi keyystore da ubacimo CA");
 								
 						//dodajemo u vec postojeci keystore
 						dialogGetPassword = new DialogGetPassword(MainFrame.getInstance(),
 								"Subject pass of issuer certificat", x509certificate, keyPair,
 								ca,givenname);
 						dialogGetPassword.setVisible(true);
 					}
					closeDialog();
				}
			}
		});
		JButton jbCancel = new JButton("CANCEL");
		jbCancel.addActionListener(new ActionListener() {

	 
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				closeDialog();
			}
		});
		JPanel jpButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
		jpButtons.add(jbOK);
		jpButtons.add(jbCancel);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(jpOptions, BorderLayout.CENTER);
		getContentPane().add(jpButtons, BorderLayout.SOUTH);

		setTitle(sTitle);

		getRootPane().setDefaultButton(jbOK);

		initDialog();

		// Focus common name input by default
		m_jtfSurname.requestFocusInWindow();
	}

	/*
	 * Populate the signature algorithm combo box with the signature algorithms
	 * applicable to the key pair algorithm. Also set a sane default selected
	 * item, and disable the combo box if it has less than 2 items.
	 * 
	 * @param type key pair type
	 * 
	 * @param combo the combo box to populate
	 */
	// private static void populateSigAlgs(KeyPairType type, JComboBox combo)
	// {
	// combo.removeAllItems();
	// for (SignatureType st : SignatureType.valuesFor(type))
	// {
	// combo.addItem(st);
	// }
	//
	// combo.setSelectedItem(SignatureType.defaultFor(type));
	//
	// combo.setEnabled(combo.getItemCount() > 1);
	// }
	/**
	 * Initialize the dialog.
	 */
	protected void initDialog() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent evt) {
				closeDialog();
			}
		});

		setResizable(false);

		pack();
	}

	protected void closeDialog() {
		setVisible(false);
		dispose();
	}

	private boolean validateValues() {

		m_sCommonName = validateCommonName(m_jtfCommonName.getText());
		m_sOrganizationUnit = validateOrganisationUnit(m_jtfOrganisationUnit
				.getText());
		m_sOrganizationName = validateOrganisationName(m_jtfOrganisationName
				.getText());
		m_sGivenName = validateLocalityName(m_jtfGivenName.getText());
		m_sStateName = validateStateName(m_jtfStateName.getText());
		m_sCountryCode = validateCountryCode(m_jtfCountryCode.getText());
		m_sEmailAddress = validateEmailAddress(m_jtfEmailAddress.getText());

		if (m_sCommonName == null || m_sOrganizationUnit == null
				 || m_sOrganizationName == null  || m_sGivenName == null
						 || m_sStateName == null  || m_sCountryCode == null
								 ||m_sEmailAddress == null) {
			JOptionPane.showMessageDialog(this,
					"Please entry all",
					getTitle(), JOptionPane.WARNING_MESSAGE);
			return false;
		}

		return true;
	}

	/**
	 * Validate the supplied Common Name value.
	 * 
	 * @param sCommonName
	 *            The Validity value
	 * @return The Common Name value or null if it is not valid
	 */
	private String validateCommonName(String sCommonName) {
		sCommonName = sCommonName.trim();

		if (sCommonName.isEmpty()) {
			return null;
		}

		return sCommonName;

	}

	/**
	 * Validate the supplied Organization Unit value.
	 * 
	 * @param sOrganisationUnit
	 *            The Organization Unit value
	 * @return The Organization Unit value or null if it is not valid
	 */
	private String validateOrganisationUnit(String sOrganisationUnit) {
		sOrganisationUnit = sOrganisationUnit.trim();

		if (sOrganisationUnit.isEmpty()) {
			return null;
		}

		return sOrganisationUnit;
	}

	/**
	 * Validate the supplied Organization Name value.
	 * 
	 * @param sOrganisationName
	 *            The Organization Unit value
	 * @return The Organization Name value or null if it is not valid
	 */
	private String validateOrganisationName(String sOrganisationName) {
		 
		if (sOrganisationName.trim().isEmpty()) {
			return null;
		}

		return sOrganisationName;
	}

	/**
	 * Validate the supplied Locality Name value.
	 * 
	 * @param sLocalityName
	 *            The Locality Name value
	 * @return The Locality Name value or null if it is not valid
	 */
	private String validateLocalityName(String sLocalityName) {
		sLocalityName = sLocalityName.trim();

		if (sLocalityName.isEmpty()) {
			return null;
		}

		return sLocalityName;
	}

	/**
	 * Validate the supplied State Name value.
	 * 
	 * @param sStateName
	 *            The State Name value
	 * @return The State Name value or null if it is not valid
	 */
	private String validateStateName(String sStateName) {
		sStateName = sStateName.trim();

		if (sStateName.isEmpty()) {
			return null;
		}

		return sStateName;
	}

	/**
	 * Validate the supplied Country Code value.
	 * 
	 * @param sCountryCode
	 *            The Country Code value
	 * @return The Country Code value or null if it is not valid
	 */
	private String validateCountryCode(String sCountryCode) {
		sCountryCode = sCountryCode.trim();

		if (sCountryCode.isEmpty()) {
			return null;
		}

		return sCountryCode;
	}

	/**
	 * Validate the supplied Email Address value.
	 * 
	 * @param sEmailAddress
	 *            The Email Address value
	 * @return The Email Address value or null if it is not valid
	 */
	private String validateEmailAddress(String sEmailAddress) {
		sEmailAddress = sEmailAddress.trim();

		if (sEmailAddress.isEmpty()) {
			return null;
		}

		return sEmailAddress;
	}

	private ArrayList<String> allCN() {
 		ArrayList<String> results = new ArrayList<String>();
//		//results.add("");
//
//		File[] files = new File(KEY_STORE_FILE).listFiles();
//		// If this pathname does not denote a directory, then listFiles()
//		// returns null.
//
//		for (File file : files) {
//			if (file.getName().contains(".cer")) {
//				results.add(file.getName().replace(".cer","").trim());
//			}
//		}
//		return results;
 		for (Entry<Integer, KeySert> entry : MainFrame.getInstance().getKeySCertificates().getMapaKS().entrySet()) {
 			results.add(entry.getValue().getCertificate());
		     
		} 
 		results.add("");
 		return results;
	}

}
