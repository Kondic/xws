package gui ;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class ToolbarStandard extends JToolBar {
	
	private JButton genKp;

	 
	DialogGenerateCertificate dialogGenerateCertificate;
	

	
	public  ToolbarStandard(){ 
		super(SwingConstants.HORIZONTAL);
		setFloatable(false);//nepokretan
		setBackground(Color.gray);
		 
		genKp = new JButton();
		genKp.setIcon(new ImageIcon("icons/2_application-pgp-signature.png"));
		genKp.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				dialogGenerateCertificate = new DialogGenerateCertificate(
						MainFrame.getInstance(), "Entry for certificate");
				dialogGenerateCertificate.setVisible(true);

			}
		});
	 	add(genKp);

		addSeparator();
		 
		 
	}
	
 
}
