package gui;

import gui.tablemodel.KeySert;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.security.KeyPair;
import java.security.cert.X509Certificate;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import rs.ac.uns.ftn.informatika.ib.crypto.KeyStoreWrapper;

@SuppressWarnings("serial")
public class DialogGetPassword extends JDialog {

	private X509Certificate x509Certificate;
	private KeyPair keyPair;
	private String givenname;
	private String certname;
	private DialogAlias dialogAlias;
	/** First password entry password field */
	private JPasswordField m_jpfFirst;

	/** Password confirmation entry password field */
	private JPasswordField m_jpfConfirm;

	/** Stores new password entered */
	private char[] m_cPassword;

	/**
	 * Creates new DGetNewPassword dialog.
	 * 
	 * @param parent
	 *            Parent window
	 * @param sTitle
	 *            The dialog's title
	 */
	public DialogGetPassword(Window parent, String sTitle, String alias) {
		super(parent, sTitle);
		initComponents();
		setLocationRelativeTo(null);
	}

	public DialogGetPassword(Window parent, String sTitle,
			X509Certificate x509certificatep, KeyPair keyPairp,
			String givennamep,String certnamep) {
		// TODO Auto-generated constructor stub
		super(parent, sTitle);
		keyPair = keyPairp;
		x509Certificate = x509certificatep;
		givenname = givennamep;
		certname=certnamep;
		initComponents();
		setLocationRelativeTo(null);
	}

	/**
	 * Get the password set in the dialog.
	 * 
	 * @return The password or null if none was set
	 */
	public char[] getPassword() {
		if (m_cPassword == null) {
			return null;
		}
		char[] copy = new char[m_cPassword.length];
		System.arraycopy(m_cPassword, 0, copy, 0, copy.length);
		return copy;
	}

	/**
	 * Initialize the dialog's GUI components.
	 */
	private void initComponents() {
		getContentPane().setLayout(new BorderLayout());

		JLabel jlFirst = new JLabel("Password");
		
		m_jpfFirst = new JPasswordField(25);
		 
		jlFirst.setLabelFor(m_jpfFirst);
		

		JButton jbOK = new JButton("OK");
		jbOK.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (validatePassword()) {
					KeyStoreWrapper novi = KeyStoreWrapper.getInstance();
					KeySert kc= MainFrame.getInstance().getKeySCertificates().getKeySert(givenname);
						
					 String pwd= kc.getPasscert();
					//char[] pwd = novi.getEntryPassword(givenname);

					// dodao si i Alias
					 String passKeyS = new String(m_jpfFirst.getPassword());
					// String pass=new String(pwd);
					if (pwd.equals(passKeyS)) {
						dialogAlias = new DialogAlias(MainFrame.getInstance(),
								"Adding Alias", x509Certificate, keyPair,
								certname);
						dialogAlias.setVisible(true);
						closeDialog();
					} else {
						checkPass();
					}

					//closeDialog();
				}
			}
		});
		JButton jbCancel = new JButton("CANCEL");
		jbCancel.addActionListener(new ActionListener() {

			 
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				closeDialog();
			}
		});
		JPanel jpPassword = new JPanel(new GridLayout(2, 2, 5, 5));
		jpPassword.add(jlFirst);
		jpPassword.add(m_jpfFirst);
		
		
		jpPassword.setBorder(new EmptyBorder(5, 5, 5, 5));

		JPanel jpButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
		jpButtons.add(jbOK);
		jpButtons.add(jbCancel);

		getContentPane().add(jpPassword, BorderLayout.CENTER);
		getContentPane().add(jpButtons, BorderLayout.SOUTH);

		getRootPane().setDefaultButton(jbOK);

		initDialog();
	}

	/**
	 * Check for the following:
	 * <ul>
	 * <li>That the user has supplied and confirmed a password.
	 * <li>That the passwords match.
	 * </ul>
	 * Store the new password in this object.
	 * 
	 * @return True, if the user's dialog entry matches the above criteria,
	 *         false otherwise
	 */
	protected void checkPass() {
		JOptionPane.showMessageDialog(this, "Passwords No Match ",
				getTitle(), JOptionPane.WARNING_MESSAGE);

	}
	private boolean validatePassword() {
		String sFirstPassword = new String(m_jpfFirst.getPassword());
		 
		if (!sFirstPassword.equals("")) {
			//m_cPassword = sFirstPassword.toCharArray();
			return true;
		}

		JOptionPane.showMessageDialog(this, "Insert pass",
				getTitle(), JOptionPane.WARNING_MESSAGE);

		return false;
	}
	
	private boolean checkPassword() {
		String sFirstPassword = new String(m_jpfFirst.getPassword());
		String sConfirmPassword = new String(m_jpfConfirm.getPassword());

		if (sFirstPassword.equals(sConfirmPassword)) {
			m_cPassword = sFirstPassword.toCharArray();
			return true;
		}

		JOptionPane.showMessageDialog(this, "PasswordsNoMatch.message",
				getTitle(), JOptionPane.WARNING_MESSAGE);

		return false;
	}

	protected void closeDialog() {
		setVisible(false);
		dispose();
	}

	protected void initDialog() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent evt) {
				closeDialog();
			}
		});

		setResizable(false);

		pack();
	}
}
