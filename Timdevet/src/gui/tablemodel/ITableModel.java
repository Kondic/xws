package gui.tablemodel;


 
public interface ITableModel {

	/*
	 *  Vraca broj elemenata u hash mapi
	 */
	int getRowCount();
	
	
	/*
	 *  Vraca vrednost za zadati red i kolonu 
	 */
	Object getValueAt(int rowIndex, int columnIndex);

}
