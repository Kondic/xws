package gui.tablemodel;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

public class KeySert implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9076530943070258708L;
	private String keystore;
	private String passkey;
	private String certificate;
	private String passcert;
	private int id;
	
	private static final AtomicInteger count = new AtomicInteger(0); 
	  
	 
	public KeySert(String keystore,String passkey, String certificate ,String passcert) {
		super();
		this.keystore = keystore;
		this.passcert=passcert;
		this.passkey=passkey;
		this.certificate = certificate;
		this.id = count.incrementAndGet();
	}
	 
	
	public KeySert(String keystore, String passkey, String certificate,
			String passcert, int id) {
		super();
		this.keystore = keystore;
		this.passkey = passkey;
		this.certificate = certificate;
		this.passcert = passcert;
		this.id = id;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKeystore() {
		return keystore;
	}
	public void setKeystore(String keystore) {
		this.keystore = keystore;
	}
	public String getCertificate() {
		return certificate;
	}
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	public String getPasskey() {
		return passkey;
	}
	public void setPasskey(String passkey) {
		this.passkey = passkey;
	}
	public String getPasscert() {
		return passcert;
	}
	public void setPasscert(String passcert) {
		this.passcert = passcert;
	}
	
	
}
