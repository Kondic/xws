package gui.tablemodel;

import javax.swing.table.AbstractTableModel;

 
@SuppressWarnings("serial")
public class DialogTableModel extends AbstractTableModel {

	private String[] columnNames ;
    private ITableModel itableModel;

    public DialogTableModel(String[] columnNames, ITableModel itableModel) {
    	this.columnNames = columnNames;
    	this.itableModel = itableModel;
   
    }
    
    public String getColumnName(int col) {
        return columnNames[col];
    }

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return itableModel.getRowCount();
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		return itableModel.getValueAt(rowIndex, columnIndex);
	}

}
