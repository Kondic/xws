package gui.tablemodel;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

 
 

public class KeySCertificates implements ITableModel {
	 
	private HashMap<Integer ,KeySert  > mapaKS = new HashMap<Integer ,KeySert  >();
	public void addKS(KeySert novi)  {
		 mapaKS.put(novi.getId(), novi);
	}
	
	public void removeKS(KeySert stari)   {
		 	mapaKS.remove(stari.getId());
	}

	public int getRowCount() {
		// TODO Auto-generated method stub
		return mapaKS.size();
	}
	
	public KeySert getKeySert(String certificate){
		KeySert a=null;
		for (Entry<Integer, KeySert> entry : mapaKS.entrySet()) {
		    if(entry.getValue().getCertificate().equals(certificate)){
		    	a=entry.getValue();
		        return a;
		    }
		} 
		 return a;
	}
	
	 
	
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		ArrayList<KeySert> keySert = new ArrayList<KeySert>(mapaKS.values());
		switch(columnIndex){
			case 0: return keySert.get(rowIndex).getId();
			case 1: return keySert.get(rowIndex).getKeystore();
			case 2: return keySert.get(rowIndex).getCertificate();
		}
		return "";
	}

	public HashMap<Integer, KeySert> getMapaKS() {
		return mapaKS;
	}

	public void setMapaKS(HashMap<Integer, KeySert> mapaKS) {
		this.mapaKS = mapaKS;
	}
	
	public void setNew(){
		mapaKS = new HashMap<Integer ,KeySert  >();
	}
	
	public void save(ObjectOutputStream out) {
		try {
			out.writeObject(mapaKS);
			out.flush();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	public void load(ObjectInputStream in) {
		try {
			mapaKS = (HashMap<Integer ,KeySert  >) in.readObject();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 
	}	
 	  
}
