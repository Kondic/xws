package gui;


 
import gui.tablemodel.DialogTableModel;
import gui.tablemodel.KeySCertificates;
import gui.tablemodel.KeySert;

import java.awt.BorderLayout;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;



public class MainFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4293551756673209121L;
	private MyMenu menu;
	private StatusBar statusBar;
	private ToolbarStandard toolbarStandard;
	private JTable table;
	private static MainFrame instance = null;
	private KeySCertificates keySCertificates = new KeySCertificates();
	protected DialogTableModel tableModel;
	public static String filename = "aplikacija.txt";
	
	public static MainFrame getInstance() {
		if (instance == null) {
			  instance = new MainFrame();
			  
			 
		}
		 instance.initGUI();
		return instance;
	}

	public KeySCertificates getKeySCertificates() {
		return keySCertificates;
	}

	public void setKeySCertificates(KeySCertificates keySCertificates) {
		this.keySCertificates = keySCertificates;
	}
	
	public void addKS(KeySert novi){
		 keySCertificates.addKS(novi);
	}

	 
	public void initGUI() {
		//setIconImage(new ImageIcon("icons/2_application-pgp-signature.png"));
		 ImageIcon icon = new ImageIcon(new ImageIcon("icons/2_application-pgp-signature.png").getImage());
	     setIconImage(icon.getImage());
	     
		// Toolkit kit = Toolkit.getDefaultToolkit();
		// Dimension screenSize = kit.getScreenSize();
		// int screenHeight = screenSize.height;
		// int screenWidth = screenSize.width;
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth();
		int height = gd.getDisplayMode().getHeight();
		setSize(3 * width / 5, 3 * height / 5);
		// setSize(screenWidth *3/ 4, screenHeight *3/ 4);
		setTitle("Tim devet");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 
		menu = new MyMenu();
		setJMenuBar(menu);

		statusBar = new StatusBar();

		toolbarStandard = new ToolbarStandard();
		add(toolbarStandard, BorderLayout.NORTH);
		add(statusBar, BorderLayout.SOUTH);
		String[] tableColumns = new String[3];
		tableColumns[0] = "id";
		tableColumns[1] = "keystore";
		tableColumns[2] = "certificate";
		 tableModel = new DialogTableModel(tableColumns, keySCertificates);

		table = new JTable(tableModel);
		table.setAutoCreateRowSorter(true); // sortira elemente tabele po
											// izabranoj koloni
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setFillsViewportHeight(true);
		 table.getColumnModel().getColumn(0).setMinWidth(150);
		 table.getColumnModel().getColumn(1).setMinWidth(250);
		 table.getColumnModel().getColumn(2).setMinWidth(250);
		JScrollPane scrollPane = new JScrollPane(table);

		// Add the scroll pane to this panel.
		add(scrollPane);
		setLocationRelativeTo(null);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addWindowListener(new MainFrameListener());

	}
	
	public void save(){
//		if(instance != null){
//			System.out.println("saving...");
//			try{
//				FileOutputStream fos = new FileOutputStream(MainFrame.filename);
//				ObjectOutputStream oos = new ObjectOutputStream(fos);
//				oos.writeObject(instance);
//				oos.close();
//				 
//			} catch (Exception ex) {
//				ex.getMessage();
//			}
//		}
		 
				try {
					ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("keySCertificates.dat"));
					keySCertificates.save(out);
					out.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
		
 	//sprivate static MainFrame loading(){
		
//		System.out.println("Loading...");
//		MainFrame ap = null;
//		try{
//			FileInputStream fis = new FileInputStream(MainFrame.filename);
//			ObjectInputStream ois = new ObjectInputStream(fis);
//			ap = (MainFrame) ois.readObject();
//			ois.close();
//		} catch (Exception ex) {
//			ex.getMessage();
//		}
//		return ap;
	public void load()  {
		try {
			File file = new File("keySCertificates.dat");
			if (file.exists()) {
				ObjectInputStream in = new ObjectInputStream(new FileInputStream("keySCertificates.dat"));
				keySCertificates.load(in);
				in.close();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 
	 
 	class MainFrameListener implements WindowListener {

		@Override
		public void windowActivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosed(WindowEvent e) {
			// TODO Auto-generated method stub
			 
		}

		@Override
		public void windowClosing(WindowEvent e) {
			MainFrame.getInstance().save();
		 
			 
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowOpened(WindowEvent e) {
			 MainFrame.getInstance().load();
		}
		
	}		
}
