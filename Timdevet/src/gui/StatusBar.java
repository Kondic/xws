package gui ;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.text.DateFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

@SuppressWarnings("serial")
public class StatusBar extends JPanel{
	
	
	
	private StatusPane statusBarMenuDescription;
	private StatusPane statusBarLanguage;
	private StatusPane statusAuthor;
	
	public StatusBar(){
		
		
		setLayout(new GridLayout(1,3,5,5));
		setBackground(Color.lightGray);
		setBorder(BorderFactory.createLineBorder(Color.darkGray));
		
		  
		
		statusAuthor=new StatusPane("Tim Devet");
		DateFormat df=DateFormat.getDateInstance();
		String datum=df.format(new Date());
		statusBarMenuDescription=new StatusPane(datum);
		
		add(statusAuthor);
		 
		add(statusBarMenuDescription);
	}

	
	 class StatusPane extends JLabel{
		
		public StatusPane(String text) {
			super(text);
			setHorizontalAlignment(CENTER);
			setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
			setPreferredSize(new Dimension(200,25));
			
		}
		
		
		
	}

	public void initComponents(String kor){
		
		statusBarLanguage.setText( "lblJezik" );

		
		DateFormat df=DateFormat.getDateInstance();
		String datum=df.format(new Date());
		statusBarMenuDescription.setText(datum);
		statusAuthor.setText( "lblKorisnik"   );
		
	}


 


 
}
