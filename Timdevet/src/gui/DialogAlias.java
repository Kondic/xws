package gui;

import gui.tablemodel.KeySert;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.security.KeyPair;
import java.security.cert.X509Certificate;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import rs.ac.uns.ftn.informatika.ib.crypto.KeyStoreWrapper;
import rs.ac.uns.ftn.informatika.ib.security.KeyStoreWriter;

public class DialogAlias extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6752363669378306532L;

	DialogGetPassword dialogGetPassword;

	/** Alias text field */
	private JTextField m_jtfAlias;

	/** Stores the alias entered by the user */
	// private String m_sAlias;
	private String givenname;
	/** First password entry password field */
	private JPasswordField m_jpfFirst;

	/** Password confirmation entry password field */
	private JPasswordField m_jpfConfirm;

	private X509Certificate x509Certificate;
	private KeyPair keyPair;

	/**
	 * Creates new DGetAlias dialog.
	 * 
	 * @param parent
	 *            The parent window
	 * @param sTitle
	 *            The dialog's title
	 * @param sOldAlias
	 *            The alias to display initially
	 * @param select
	 *            Whether to pre-select the initially displayed alias
	 */
	public DialogAlias(Window parent, String sTitle,
			X509Certificate x509CertificateP, KeyPair keyPairP,
			String pgivenname) {
		super(parent, sTitle);
		initComponents();
		x509Certificate = x509CertificateP;
		keyPair = keyPairP;
		givenname = pgivenname;
		setLocationRelativeTo(null);
	}

	/**
	 * Initialize the dialog's GUI components.
	 * 
	 * @param sOldAlias
	 *            The alias to display initially
	 * @param select
	 *            Whether to pre-select the initially displayed alias
	 */
	private void initComponents() {
		getContentPane().setLayout(new BorderLayout());

		JLabel jlAlias = new JLabel("Alias");
		m_jtfAlias = new JTextField(15);

		jlAlias.setLabelFor(m_jtfAlias);

		JLabel jlFirst = new JLabel("Password for Key Store");
		JLabel jlConfirm = new JLabel("Password for Alias(CERT)");
		m_jpfFirst = new JPasswordField(15);
		m_jpfConfirm = new JPasswordField(15);
		jlFirst.setLabelFor(m_jpfFirst);
		jlConfirm.setLabelFor(m_jpfConfirm);

		JButton jbOK = new JButton("OK");
		jbOK.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (checkPassword()) {
					// dodao si i Alias
					// kreira se keystore, ucitava ks fajl, dodaje kljuc i
					// sertifikat i sacuvaju se izmene
					String alias = m_jtfAlias.getText().trim();
					String passAlias = new String(m_jpfConfirm.getPassword());
					String passKeyS = new String(m_jpfFirst.getPassword());

					KeyStoreWriter keyStoreWriter = new KeyStoreWriter();
					keyStoreWriter.loadKeyStore(null, passKeyS.toCharArray());
					keyStoreWriter.write(givenname, keyPair.getPrivate(),
							passAlias.toCharArray(), x509Certificate);
					keyStoreWriter.saveKeyStore("./data/" + alias + ".jks",
							passKeyS.toCharArray());
					
					
					MainFrame.getInstance().addKS(new KeySert(alias,passKeyS ,givenname,passAlias,MainFrame.getInstance().getKeySCertificates().getRowCount()+1));
					MainFrame.getInstance().initGUI();   
//					KeyStoreWrapper novi = KeyStoreWrapper.getInstance(
//							new File("./data/" + alias + ".jks"),
//							passKeyS.toCharArray());
//					novi.setEntryPassword(givenname, passAlias.toCharArray());
//					
					closeDialog();
				}
			}
		});
		JButton jbCancel = new JButton("CANCEL");
		jbCancel.addActionListener(new ActionListener() {

			 
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				closeDialog();
			}
		});

		JPanel jpAlias = new JPanel(new GridLayout(3, 2));
		jpAlias.add(jlAlias);
		jpAlias.add(m_jtfAlias);
		jpAlias.add(jlFirst);
		jpAlias.add(m_jpfFirst);
		jpAlias.add(jlConfirm);
		jpAlias.add(m_jpfConfirm);
		jpAlias.setBorder(new EmptyBorder(5, 5, 5, 5));

		JPanel jpButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
		jpButtons.add(jbOK);
		jpButtons.add(jbCancel);

		getContentPane().add(jpAlias, BorderLayout.CENTER);
		getContentPane().add(jpButtons, BorderLayout.SOUTH);

		getRootPane().setDefaultButton(jbOK);

		initDialog();

	}

	protected void initDialog() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent evt) {
				closeDialog();
			}
		});

		setResizable(false);

		pack();
	}

	protected void closeDialog() {
		setVisible(false);
		dispose();
	}

	private boolean checkPassword() {
		String sFirstPassword = new String(m_jpfFirst.getPassword());
		String sConfirmPassword = new String(m_jpfConfirm.getPassword());
		String alias= m_jtfAlias.getText().trim();
		if (!alias.equals("") && !sFirstPassword.equals("") && !sConfirmPassword.equals("")) {

			return true;
		}

		JOptionPane.showMessageDialog(this, "Insert both passswords and alias",
				getTitle(), JOptionPane.WARNING_MESSAGE);

		return false;
	}
}
