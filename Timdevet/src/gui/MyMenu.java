package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

@SuppressWarnings("serial")
public class MyMenu extends JMenuBar {

	JMenu mnuFile;
	JMenuItem mniFile;
	JMenuItem mniGenerate;
	JMenuItem mniDelete;
	DialogGenerateCertificate dialogGenerateCertificate;
	boolean firsttime = false;

	public MyMenu() {

		mnuFile = new JMenu("File");
		mnuFile.setMnemonic(KeyEvent.VK_H);
		add(mnuFile);

		mniFile = new JMenuItem("File");
		mniFile.setAccelerator(KeyStroke.getKeyStroke("control shift F"));
		mniFile.setMnemonic(KeyEvent.VK_R);

		mniGenerate = new JMenuItem("Generate Certificate");
		mniGenerate.setAccelerator(KeyStroke.getKeyStroke("control shift G"));
		mniGenerate.setMnemonic(KeyEvent.VK_R);
		mniGenerate.setIcon(new ImageIcon("icons/1_Key.png"));
		mniGenerate.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				dialogGenerateCertificate = new DialogGenerateCertificate(
						MainFrame.getInstance(), "Entry for certificate");
				dialogGenerateCertificate.setVisible(true);

			}
		});
		mniDelete = new JMenuItem("Delete");
		mniDelete.setAccelerator(KeyStroke.getKeyStroke("control shift D"));
		mniDelete.setMnemonic(KeyEvent.VK_D);
		mniDelete.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				 MainFrame.getInstance().getKeySCertificates().setNew();
				 
			}
		});
		
		mnuFile.add(mniFile);
		mnuFile.add(mniGenerate);
		mnuFile.add(mniDelete);
	}

}
