/*
 * KeyStoreWrapper.java
 * This file is part of Portecle, a multipurpose keystore and certificate tool.
 *
 * Copyright © 2004 Wayne Grant, waynedgrant@hotmail.com
 *             2006 Ville Skyttä, ville.skytta@iki.fi
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package rs.ac.uns.ftn.informatika.ib.crypto;

import java.io.File;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
 

/**
 * Wrapper class for a keystore. Used to keep a track of the keystore's physical file, its password, the
 * password's of its protected entries and whether or not the keystore has been changed since it was last
 * saved.
 */
public class KeyStoreWrapper
{
	/** The wrapped keystore */
	private KeyStore m_keyStore;

	 
	/** The keystore's password */
	private char[] m_cPassword;

	/** Keystore entry passwords */
	private HashMap<String, char[]> m_mPasswords = new HashMap<String, char[]>();

	/** File the keystore was loaded from/saved to */
	private File m_fKeyStore;

	/**
	 * Indicator as to whether or not the keystore has been altered since its last save
	 */
	private boolean m_bChanged;

	 

	/**
	 * Construct a new KeyStoreWrapper for the supplied keystore, keystore file and keystore password.
	 * 
	 * @param keyStore The keystore
	 * @param fKeyStore The keystore file
	 * @param cPassword The keystore password
	 */
private static KeyStoreWrapper instance=null;
	
	public static KeyStoreWrapper getInstance(){
		if (instance == null){
			//System.out.println("vraca nov!!!");
			instance = new KeyStoreWrapper();
		 
		}
		 return instance;
	}
	public static KeyStoreWrapper getInstance(  File fKeyStore, char[] cPassword){
		if (instance == null){
			instance = new KeyStoreWrapper(    fKeyStore,  cPassword);
			 
		}
		 return instance;
	}
	
	public KeyStoreWrapper(KeyStore keyStore, File fKeyStore, char[] cPassword)
	{
		this(keyStore);
		this.m_fKeyStore = fKeyStore;
		this.m_cPassword = cPassword;
	}
	
	public KeyStoreWrapper( File fKeyStore, char[] cPassword)
	{
		this();
		this.m_fKeyStore = fKeyStore;
		this.m_cPassword = cPassword;
	}
	/**
	 * Construct a new KeyStoreWrapper for the supplied keystore.
	 * 
	 * @param keyStore The keystore
	 */
	public KeyStoreWrapper(KeyStore keyStore)
	{
		setKeyStore(keyStore);
	}
	public KeyStoreWrapper( )
	{
		
	}
	public void setKeyStore(KeyStore keyStore)
	{
		m_keyStore = keyStore;
	 
	}
	public void setKeyStore( )
	{
		try {
			this.m_keyStore=KeyStore.getInstance("JKS", "SUN");
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Set the password for a particular keystore entry in the wrapper.
	 * 
	 * @param sAlias The keystore entry's alias
	 * @param cPassword The keystore entry's password
	 */
	
	
	public void setEntryPassword(String sAlias, char[] cPassword)
	{
		this.m_mPasswords.put(sAlias, cPassword);
	}
	
	 public HashMap<String, char[]> getM_mPasswords() {
		return m_mPasswords;
	}
	public void setM_mPasswords(HashMap<String, char[]> m_mPasswords) {
		this.m_mPasswords = m_mPasswords;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "name of file:"+m_fKeyStore.getName()+"||pass keystore:"+m_cPassword.toString()+"||mapa:"+m_mPasswords.toString() ;
	}

	/**
	 * Remove a particular keystore entry from the wrapper.
	 * 
	 * @param sAlias The keystore entry's alias
	 */
	public void removeEntryPassword(String sAlias)
	{
		m_mPasswords.remove(sAlias);
	}

	/**
	 * Get the password for a particular keystore entry.
	 * 
	 * @param sAlias The keystore entry's alias
	 * @return The keystore entry's password or null if none is set
	 */
	public char[] getEntryPassword(String sAlias)
	{
		return this.m_mPasswords.get(sAlias);
	}

	/**
	 * Get the keystore's physical file.
	 * 
	 * @return The keystore entry's physical file or null if none is set
	 */
	public File getKeyStoreFile()
	{
		return this.m_fKeyStore;
	}

	/**
	 * Set the keystore's physical file in the wrapper.
	 * 
	 * @param fKeyStore The keystore entry's physical file
	 */
	public void setKeyStoreFile(File fKeyStore)
	{
		this.m_fKeyStore = fKeyStore;
	}

	/**
	 * Get the keystore.
	 * 
	 * @return The keystore
	 */
	public KeyStore getKeyStore()
	{
		 return this.m_keyStore;
	}

	 

 
	/**
	 * Get the keystore password
	 * 
	 * @return The keystore password
	 */
	public char[] getPassword()
	{
		return m_cPassword;
	}

	/**
	 * Set the keystore password in the wrapper.
	 * 
	 * @param cPassword The keystore password
	 */
	public void setPassword(char[] cPassword)
	{
		m_cPassword = cPassword;
	}

	/**
	 * Register with the wrapper whether the keystore has been changed since its last save.
	 * 
	 * @param bChanged Has the keystore been changed?
	 */
	public void setChanged(boolean bChanged)
	{
		m_bChanged = bChanged;
	}

	/**
	 * Has the keystore been changed since its last save?
	 * 
	 * @return True if it has been changed, false otherwise
	 */
	public boolean isChanged()
	{
		return m_bChanged;
	}
	
	
}
