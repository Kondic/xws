package rs.ac.uns.ftn.informatika.ib.security;

import gui.MainFrame;
import gui.tablemodel.KeySert;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import rs.ac.uns.ftn.informatika.ib.crypto.KeyStoreWrapper;

public class CertificateGenerator {

	private static final String KEY_STORE_FILE = "./data/";
	// registracija providera

	/** KeystoreWrapper object containing the current keystore */
	private KeyStoreWrapper m_keyStoreWrap;

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	public X509Certificate generateCertificate(IssuerData issuerData,
			SubjectData subjectData) {
		try {

			// posto klasa za generisanje sertifiakta ne moze da primi direktno
			// privatni kljuc
			// pravi se builder za objekat koji ce sadrzati privatni kljuc i
			// koji
			// ce se koristitit za potpisivanje sertifikata
			// parametar je koji algoritam se koristi za potpisivanje
			// sertifiakta
			JcaContentSignerBuilder builder = new JcaContentSignerBuilder(
					"SHA256WithRSAEncryption");
			// koji provider se koristi
			builder = builder.setProvider("BC");

			// objekat koji ce sadrzati privatni kljuc i koji ce se koristiti za
			// potpisivanje sertifikata
			ContentSigner contentSigner = builder.build(issuerData
					.getPrivateKey());

			// postavljaju se podaci za generisanje sertifiakta
			X509v3CertificateBuilder certGen = new JcaX509v3CertificateBuilder(
					issuerData.getX500name(), new BigInteger(
							subjectData.getSerialNumber()),
					subjectData.getStartDate(), subjectData.getEndDate(),
					subjectData.getX500name(), subjectData.getPublicKey());
			// generise se sertifikat
			X509CertificateHolder certHolder = certGen.build(contentSigner);

			// certGen generise sertifikat kao objekat klase
			// X509CertificateHolder
			// sad je potrebno certHolder konvertovati u sertifikat
			// za to se koristi certConverter
			JcaX509CertificateConverter certConverter = new JcaX509CertificateConverter();
			certConverter = certConverter.setProvider("BC");

			// konvertuje objekat u sertifikat i vraca ga
			return certConverter.getCertificate(certHolder);

		} catch (CertificateEncodingException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return null;
		} catch (OperatorCreationException e) {
			e.printStackTrace();
			return null;
		} catch (CertificateException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void saveCert(X509Certificate x509Certificat, String ime) {

		try {
			System.out.println("CUVANJEEEEEEEEEEEEEEEEEEEEEee");
			final FileOutputStream os = new FileOutputStream("./data/" + ime
					+ ".cer");
			os.write("-----BEGIN CERTIFICATE-----\n".getBytes("US-ASCII"));
			os.write(Base64.encodeBase64(x509Certificat.getEncoded(), true));
			os.write("-----END CERTIFICATE-----\n".getBytes("US-ASCII"));
			os.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public KeyPair generateKeyPair() {
		try {
			// generator para kljuceva
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			// inicijalizacija generatora, 1024 bitni kljuc
			keyGen.initialize(1024);

			// generise par kljuceva
			KeyPair pair = keyGen.generateKeyPair();

			return pair;

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	public X509Certificate generateSsCertificateFromScratch(String CA,
			Date startDate, Date endDate,   String cn,
			String surname, String givenname, String o, String ou, String c,
			String e, KeyPair keyPair) {

		// datumi
		SimpleDateFormat iso8601Formater = new SimpleDateFormat("yyyy-MM-dd");
		// Date startDate = iso8601Formater.parse("2007-12-31");
		//
		// Date endDate = iso8601Formater.parse("2017-12-31");
		//
		// podaci o vlasniku i izdavacu posto je self signed
		// klasa X500NameBuilder pravi X500Name objekat koji nam treba
		X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
		builder.addRDN(BCStyle.UID, "123445");
		
		builder.addRDN(BCStyle.CN, cn);
		builder.addRDN(BCStyle.SURNAME, surname);
		builder.addRDN(BCStyle.GIVENNAME, givenname);
		builder.addRDN(BCStyle.O, o);
		builder.addRDN(BCStyle.OU, ou);
		builder.addRDN(BCStyle.C, c);
		builder.addRDN(BCStyle.E, e);

		// UID (USER ID) je ID korisnika
		 

		// Serijski broj sertifikata
		String sn = "1";
		System.out.println("CA:" + CA);

		IssuerData issuerData;
		if (CA != null && !CA.equals("")) {
			// char[] cPassword = null;
			System.out.println("Upao");
			issuerData = getIssuerData(CA);
		} else {
			issuerData = new IssuerData(keyPair.getPrivate(), builder.build());
		}
		// kreiraju se podaci za issuer-a
		// IssuerData issuerData = new IssuerData(keyPair.getPrivate(),
		// builder.build());
		// kreiraju se podaci za vlasnika
		SubjectData subjectData = new SubjectData(keyPair.getPublic(),
				builder.build(), sn, startDate, endDate);

		// generise se sertifikat
		X509Certificate cert = generateCertificate(issuerData, subjectData);
		System.out
				.println("ISSUER: " + cert.getIssuerX500Principal().getName());
		System.out.println("SUBJECT: "
				+ cert.getSubjectX500Principal().getName());
		System.out.println("Sertifikat:");
		System.out
				.println("-------------------------------------------------------");
		System.out.println(cert);
		System.out
				.println("-------------------------------------------------------");
		// ako validacija nije uspesna desice se exception

		// // ovde bi trebalo da prodje
		// try {
		// cert.verify(keyPair.getPublic());
		// } catch (InvalidKeyException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// } catch (CertificateException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// } catch (NoSuchAlgorithmException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// } catch (NoSuchProviderException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// } catch (SignatureException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }
		// System.out.println("VALIDACIJA USPESNA....");

		return cert;

	}

	public void testIt() {
		// kreira se self signed sertifikat
		try {
			// par kljuceva
			KeyPair keyPair = generateKeyPair();

			// datumi
			SimpleDateFormat iso8601Formater = new SimpleDateFormat(
					"yyyy-MM-dd");
			Date startDate = iso8601Formater.parse("2007-12-31");
			Date endDate = iso8601Formater.parse("2017-12-31");

			// podaci o vlasniku i izdavacu posto je self signed
			// klasa X500NameBuilder pravi X500Name objekat koji nam treba
			X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
			builder.addRDN(BCStyle.CN, "Goran Sladic");
			builder.addRDN(BCStyle.SURNAME, "Sladic");
			builder.addRDN(BCStyle.GIVENNAME, "Goran");
			builder.addRDN(BCStyle.O, "UNS-FTN");
			builder.addRDN(BCStyle.OU, "Katedra za informatiku");
			builder.addRDN(BCStyle.C, "RS");
			builder.addRDN(BCStyle.E, "sladicg@uns.ac.rs");

			// UID (USER ID) je ID korisnika
			builder.addRDN(BCStyle.UID, "123445");
		//	BCStyle.
			// Serijski broj sertifikata
			String sn = "1";
			// kreiraju se podaci za issuer-a
			IssuerData issuerData = new IssuerData(keyPair.getPrivate(),
					builder.build());
			// kreiraju se podaci za vlasnika
			SubjectData subjectData = new SubjectData(keyPair.getPublic(),
					builder.build(), sn, startDate, endDate);

			// generise se sertifikat
			X509Certificate cert = generateCertificate(issuerData, subjectData);

			System.out.println("ISSUER: "
					+ cert.getIssuerX500Principal().getName());
			System.out.println("SUBJECT: "
					+ cert.getSubjectX500Principal().getName());
			System.out.println("Sertifikat:");
			System.out
					.println("-------------------------------------------------------");
			System.out.println(cert);
			System.out
					.println("-------------------------------------------------------");
			// ako validacija nije uspesna desice se exception

			// ovde bi trebalo da prodje
			cert.verify(keyPair.getPublic());
			System.out.println("VALIDACIJA USPESNA....");

			// ovde bi trebalo da se desi exception, jer validaciju vrsimo
			// drugim kljucem
			// KeyPair anotherPair = generateKeyPair();
			// cert.verify(anotherPair.getPublic());

		} catch (ParseException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		CertificateGenerator gen = new CertificateGenerator();
		gen.testIt();
	}

	public IssuerData getIssuerData(String name) {
		IssuerData issuer = null;

		//KeyStoreWrapper novi = KeyStoreWrapper.getInstance();
		 KeySert kc= MainFrame.getInstance().getKeySCertificates().getKeySert(name);
		
		//System.out.println(novi);
		//System.out.println("ime CA:"+name);
		//
		// System.out.println(pwd);
		// String nameJKS = novi.getKeyStoreFile().getName();

		String pwd = kc.getPasscert();
		//System.out.println("lozinka ca:"+pwd.toString());
		String password = kc.getPasskey();
		//System.out.println("lozinka ks:"+password.toString());
		try {
			// kreiramo instancu KeyStore
			// ucitavamo podatke
			// System.out.println(m_keyStoreWrap.getEntryPassword(name));
			 KeyStore ks =  KeyStore.getInstance("JKS");
			//KeyStore ks =novi.getKeyStore();
			//System.out.println(ks);
			// ucitavamo podatke
			BufferedInputStream in = null;
			try {
				in = new BufferedInputStream(new FileInputStream("./data/"+kc.getKeystore()+".jks"));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println("POKRENITE NOVI ");
			//	e.printStackTrace();
			}
   		System.out.println("KEYSTORE:"+"./data/"+kc.getKeystore()+".jks" );
//			System.out.println("NAZIV CA:"+name);
//			System.out.println("PASS CA:"+novi.getEntryPassword(name).toString());
//			
			try {
				ks.load(in, password.toCharArray());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			if (ks.isKeyEntry(name)) {
				Certificate cert = ks.getCertificate(name);

				PrivateKey privKey = (PrivateKey) ks.getKey(name, pwd.toCharArray());
				System.out.println(privKey);
				X500Name x500name = new JcaX509CertificateHolder(
						(X509Certificate) cert).getSubject();
				issuer = new IssuerData(privKey, x500name);
			} else {
				System.out.println("Nije pronasao kljuc.");
			}

		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		}
		return issuer;
	}

}
